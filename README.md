
Summary:
I developed a chatbot solution using Flask, SQLite, and Docker Compose to handle incoming WhatsApp messages from underprivileged women farmers. The chatbot assists farmers with queries related to poultry farming, including disease management and general guidance. Key features of the solution include:
Technologies Used: Flask for building the backend server, SQLite for storing input-response pairs locally, and Docker Compose for packaging and deployment. Twilio is used for WhatsApp integration.
Language Support: The solution supports Hindi text input and response using Unicode encoding, enabling effective communication with Hindi-speaking users.
Deployment: Docker Compose packages the Flask application and SQLite database, simplifying deployment and ensuring consistency across environments.
Functionality: The chatbot listens for incoming WhatsApp messages, processes them using Flask routes, and queries the SQLite database for matching responses. Predefined responses are stored in a CSV file, which is periodically read and updated in the SQLite database.

Documentation:
Architecture Overview:
The architecture consists of the following components:
Flask Application: Receives incoming WhatsApp messages and queries the SQLite database for responses. Periodically updates git the database from a CSV file.
SQLite Database: Stores input-response pairs locally in a lightweight database file. Updated periodically from a CSV file.
CSV File: Contains predefined responses for various queries, periodically read and updated by the Flask application.
Docker Compose: Packages the Flask application and SQLite database into a single containerized deployment unit.

Implementation Details:
Flask Application:
Flask routes handle incoming WhatsApp messages, extract text content, and query the SQLite database for responses.
Periodically updates the SQLite database from the predefined responses stored in the CSV file.
SQLite Database:
A SQLite database file stores input-response pairs, allowing efficient retrieval based on user queries.
Updated periodically from a CSV file containing predefined responses.
CSV File:
Contains predefined responses for various queries, allowing easy management and updates.
Periodically read and updated by the Flask application to synchronize with the SQLite database.
Docker Compose:
Defines a multi-container Docker application, including the Flask application and SQLite database.
Simplifies packaging and deployment, ensuring consistency across environments.

Deployment Instructions:
Clone the repository from Gitlab: [ ]
Update the Flask application configuration to specify the path to the SQLite database file (test_database.db) and the CSV file containing predefined responses.
Use Docker Compose to build and run the application:
docker-compose up --build
Verify the deployment by sending test messages to the WhatsApp number +14155238886 by “join friendly-ancient”.
Then start the conversation by typing “?”

Future Improvements:
Implement automated testing to ensure the reliability and correctness of the chatbot responses.
Enhance the update mechanism to handle changes in the CSV file more efficiently, such as using a file watcher or a webhook-based approach.
Integrate logging and monitoring to track application performance and user interactions for continuous improvement.



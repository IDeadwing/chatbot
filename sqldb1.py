import sqlite3
import csv
import time

def create_table():
    conn = sqlite3.connect('test_database.db')
    cur = conn.cursor()
    cur.execute('''CREATE TABLE IF NOT EXISTS intents (
                    intent TEXT PRIMARY KEY,
                    response TEXT)''')
    conn.commit()
    conn.close()

def update_database():
    # Create the table if it doesn't exist
    create_table()

    # Connect to SQLite database
    conn = sqlite3.connect('test_database.db')
    cur = conn.cursor()

    # Open and read CSV file
    with open('Qa.csv', 'r') as file:
        reader = csv.reader(file)
        next(reader)  # Skip header row
        for row in reader:
            # Check if the row has at least 2 elements
            if len(row) >= 2:
                intent = row[0].strip()  # Remove leading/trailing whitespaces
                response = row[1].strip()  # Remove leading/trailing whitespaces
                # Check if record already exists in SQLite database
                cur.execute("SELECT * FROM intents WHERE intent = ?", (intent,))
                existing_record = cur.fetchone()
                if existing_record:
                    # Update existing record
                    cur.execute("UPDATE intents SET response = ? WHERE intent = ?", (response, intent))
                else:
                    # Insert new record
                    cur.execute("INSERT INTO intents (intent, response) VALUES (?, ?)", (intent, response))

    # Commit changes and close connection
    conn.commit()
    conn.close()

# Periodically check for updates
while True:
    update_database()
    time.sleep(6)  # Check every 60 seconds

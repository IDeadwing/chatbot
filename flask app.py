from flask import Flask, request
from twilio.twiml.messaging_response import MessagingResponse
import sqlite3

app = Flask(__name__)

# Database connection
conn = sqlite3.connect('test_database.db', check_same_thread=False)
cur = conn.cursor()

triggers = ["?", ",", "#", "*", "@", "!", "_", "&", "/", ":", "+", "-"]

# Function to generate welcome message with options
def generate_welcome_message():
    welcome_msg = "स्वागत है! स्पष्टीकरण के लिए चयन करें:\n"
    cur.execute("SELECT intent FROM intents ORDER BY ROWID")  # Fetch intents in the order of their IDs
    intents = cur.fetchall()
    for index, intent in enumerate(intents, start=1):
        welcome_msg += f"{index}. {intent[0]}\n"
    welcome_msg += f"\n'0' टाइप करके बातचीत समाप्त करें।"
    welcome_msg += f"\n\nचयन करने के लिए निम्नलिखित शब्दों में से कोई एक टाइप करें:?"
    return welcome_msg

# Function to generate thank you message
def generate_thank_you_message():
    return "हमारी सेवा का उपयोग करने के लिए धन्यवाद। आपका दिन शुभ हो!"

# Function to format the response
def format_response(response):
    # Split the response by newline characters
    lines = response.split('\n')
    # Add newline after each line except the last one
    formatted_response = '\n'.join(lines[:-1]) + '\n\n' + lines[-1]
    return formatted_response

# Handle incoming messages
@app.route("/webhook", methods=["POST"])
def webhook():
    # Get incoming message
    incoming_msg = request.values.get("Body", "").lower()

    if incoming_msg in triggers:
        # Generate welcome message with options
        response = generate_welcome_message()
    elif incoming_msg == "0":
        # End the conversation with a thank you message
        response = generate_thank_you_message()
    else:
        # Retrieve response based on user's selection or intent
        try:
            selection = int(incoming_msg)
            cur.execute("SELECT response FROM intents ORDER BY ROWID")  # Fetch responses in the order of their IDs
            responses = cur.fetchall()
            if 1 <= selection <= len(responses):
                response = responses[selection - 1][0]
                response = format_response(response)
                response += "\n\nएक और क्वेरी का चयन करें या '0' टाइप करके बातचीत समाप्त करें।"
                response += "\n\nचयन करने के लिए निम्नलिखित शब्दों में से कोई एक टाइप करें:?"
            else:
                response = "अमान्य चयन। कृपया पुनः प्रयास करें।"
                response += "\n\nचयन करने के लिए निम्नलिखित शब्दों में से कोई एक टाइप करें:? "
        except ValueError:
            # Check if the input matches any intent word
            cur.execute("SELECT response FROM intents WHERE intent=?", (incoming_msg,))
            row = cur.fetchone()
            if row:
                response = row[0]
                response = format_response(response)
                response += "\n\nएक और क्वेरी का चयन करें या '0' टाइप करके बातचीत समाप्त करें।"
            else:
                # If user input is not in scope, provide a response with a helpline number as a button
                response = ("अमान्य इनपुट। कृपया बातचीत के लिए हेल्पलाइन नंबर पर कॉल करें:\n"
                            "Helpline Number: +1234567890")

    # Generate TwiML response
    twiml = MessagingResponse()
    twiml.message(response)
    return str(twiml)

# Run Flask app
if __name__ == "__main__":
    app.run(debug=True)
